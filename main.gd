extends Spatial

onready var fromContainer = $FromContainer
onready var fromText = $FromContainer/VBoxContainer/FromText
onready var dirText = $FromContainer/VBoxContainer/DirText
onready var hitContainer = $HitContainer
onready var hitText = $HitContainer/Text
onready var distContainer = $DistContainer
onready var distText = $DistContainer/Text

const Tile = preload("tile.tscn")

const WORLD_SIZE = 10
var world = []

var from # Where the ray was shot from
var dir # Which direction the ray has
var dist # Distance from `from` to floor

func create_material():
    var mat = SpatialMaterial.new()
    mat.albedo_color = Color(1, 0, 0)
    return mat
var selected_material = create_material()

func create_filled_line_material():
    var mat = SpatialMaterial.new()
    mat.albedo_color = Color(0, 1, 0)
    mat.flags_unshaded = true
    mat.flags_use_point_size = true
    return mat
var filled_line_material = create_filled_line_material()

func create_empty_line_material():
    var mat = SpatialMaterial.new()
    mat.albedo_color = Color(1, 0, 0)
    mat.flags_unshaded = true
    mat.flags_use_point_size = true
    return mat
var empty_line_material = create_empty_line_material()

var selected_tile = null
var selected_tile_material = null

const TILE_COLOR = Color("BBBBBB")
const TILE_COLOR_CONTRAST = Color("333333")

func _ready():
    var white = SpatialMaterial.new()
    white.albedo_color = TILE_COLOR
    var black = SpatialMaterial.new()
    black.albedo_color = TILE_COLOR_CONTRAST
    
    world.resize(WORLD_SIZE)
    for z in range(WORLD_SIZE):
        world[z] = []
        world[z].resize(WORLD_SIZE)
        for x in range(WORLD_SIZE):
            var tile = Tile.instance()
            tile.transform.origin = Vector3(x, 0, -z)
            var childObject = tile.get_child(0)
            if (x + z) % 2 == 0:
                childObject.material = white
            else:
                childObject.material = black
            add_child(tile)
            world[z][x] = tile.get_child(0)
            
    $Camera.transform.origin = Vector3(WORLD_SIZE / 2.0, 2.0, WORLD_SIZE / 2.0)
    $Camera.look_at(Vector3(WORLD_SIZE / 2.0, 0.0, 5), Vector3(0, 1, 0))

func update_container(container, world_pos, cam_pos, cam_dir):
    var obj_dir = world_pos - cam_pos
    if cam_dir.dot(obj_dir) > 0.0:
        container.show()
        container.set_begin($Camera.unproject_position(world_pos))
    else:
        container.hide()

func _process(delta):
    if from:
        var camera_dir = $Camera.project_ray_normal(get_viewport().get_mouse_position())
        var camera_pos = $Camera.project_ray_origin(get_viewport().get_mouse_position())
        
        update_container(fromContainer, from, camera_pos, camera_dir)
        update_container(hitContainer, from + dir * dist, camera_pos, camera_dir)
        
        # http://geomalgorithms.com/a07-_distance.html
        var a = dir.dot(dir)
        var b = dir.dot(camera_dir)
        var c = camera_dir.dot(camera_dir)
        var d = dir.dot((from - camera_pos))
        var e = camera_dir.dot( (from - camera_pos))
        var div = (a * c - b * b)
        
        var closest_dist = 0.0
        
        if div > 0.00001:
            closest_dist = (b * e - c * d) / div
            if closest_dist < 0.0:
                closest_dist = 0.0
            if closest_dist > dist:
                closest_dist = dist
            var closest_pos = from + dir * closest_dist
            
            update_container(distContainer, closest_pos, camera_pos, camera_dir)
            distText.text = "dist: %.2f, pos: %.2f %.2f %.2f" % [closest_dist, closest_pos.x, closest_pos.y, closest_pos.z]
            
            var filled_line = $FilledLine
            filled_line.material_override = filled_line_material
            filled_line.clear()
            filled_line.begin(Mesh.PRIMITIVE_LINE_STRIP, null)
            filled_line.add_vertex(from)
            filled_line.add_vertex(from + dir * closest_dist)
            filled_line.end()
            
        var empty_line = $EmptyLine
        empty_line.material_override = empty_line_material
        empty_line.clear()
        empty_line.begin(Mesh.PRIMITIVE_LINE_STRIP, null)
        empty_line.add_vertex(from + dir * closest_dist)
        empty_line.add_vertex(from + dir * dist)
        empty_line.end()

func _input(event):
    var camera = $Camera
    if event is InputEventMouseButton and event.pressed and event.button_index == 1:
        var new_from = camera.project_ray_origin(event.position)
        var new_dir = camera.project_ray_normal(event.position)
        var new_dist = -new_from.y / new_dir.y
        var hit = (new_from + new_dir * new_dist) + Vector3(0.5, 0, 0.5)
        var hit_x = floor(hit.x)
        var hit_z = -floor(hit.z)
        if hit_x >= 0 and hit_z >= 0 and hit_x < WORLD_SIZE and hit_z < WORLD_SIZE:
            if selected_tile:
                selected_tile.material = selected_tile_material
            selected_tile = world[hit_z][hit_x]
            if selected_tile:
                selected_tile_material = selected_tile.material
                selected_tile.material = selected_material
                
            from = new_from
            dir = new_dir
            dist = new_dist
            var realHit = from + dir * dist
            
            fromText.text = "from: %.2f, %.2f, %.2f" % [from.x, from.y, from.z]
            dirText.text = "dir: %.2f, %.2f, %.2f" % [dir.x, dir.y, dir.z]
            hitText.text = "hit: %.2f, %.2f, %.2f" % [realHit.x, realHit.y, realHit.z]

 
