Tile-picking demo

Used to visualize picking in a 3D environment

## Instructions

Left click to shoot a ray from the camera's current position towards where the
cursor is. The ray will be visualized, and it is possible to move the cursor to
see how `dist` affects the hit position.

## Controls:

- WASD and space/control to move camera
- Right click to enable camera look
- Left click to shoot a ray

